#!/usr/bin/env python3
# -*- coding: utf-8 -*-
###############################################################################
#       seafsh.py
#       
#       Copyright © 2015-2025, Florence Birée <florence@biree.name>
#       
#       This file is a part of python-seafile-webapi
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Seafile shell using the Seafile API wrapper"""

__author__ = "Florence Birée"
__version__ = "0.12"
__license__ = "GPLv3"
__copyright__ = "Copyright © 2015-2025, Florence Birée <florence@biree.name>"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

import os
import shutil
import shlex
import cmd
import json
from pprint import pprint
from functools import wraps
from seafile_webapi import SeafileClient 
from seafile_webapi import SeafileError, NotAuthenticated, NotFound, InvalidPath

class SeafBrowser:
    """Seafile file browser
    
        Parse seafile path under the form /library_name/path/to/a/file
        Cache file lists
        Keep a current state
    """
    
    def __init__(self, knownhosts):
        """Initialize the browser with a `knowhosts` directory"""
        self.knownhosts = knownhosts
        self.seaf = None
        self.repo_cache = []
        self.ls_cache = {} # {repo_id:{path: [{item}, {item}]}}
        self.cwd = None

    # repo and path operations

    def repo_id(self, repo_name):
        """Find a repo_id from repo_name"""
        if not self.seaf:
            raise NotAuthenticated
        if not self.repo_cache:
            self.repo_cache = self.seaf.list_repos()
        for repo in self.repo_cache:
            if repo['name'] == repo_name:
                return repo['id']
        raise NotFound

    def parse(self, seafpath):
        """Return the path under the form :
            {
                'repo_name':
                'repo_id':
                'path'
            }
            the root of all libraries has repo_id == None 
        """
        seafpath = seafpath.strip('/').split('/')
        if seafpath == ['']:
            # root
            return {'repo_name': None, 'repo_id': None, 'path': None}
        else:
            return {
                'repo_name': seafpath[0],
                'repo_id': self.repo_id(seafpath[0]),
                'path': '/' + '/'.join(seafpath[1:])
            }
    
    def join(self, *args):
        """Join all component of args. Resolve parents components."""
        # check if absolute path
        absolute = args[0].startswith('/')
        # fully split the components
        base_comp = []
        for node in args:
            base_comp += node.strip('/').split('/')
        comp = []
        # resolve parents components
        for node in base_comp:
            if node == '..':
                comp = comp[:-1]
            elif node == '':
                pass # remove //
            else:
                comp.append(node)
        return '/' + '/'.join(comp)
        
    # connection
    
    def connect(self, hostdesc):
        """Connect a Seafile instance using an host description
        
            hostdesc = {'url':, 'email':, 'password':, 'token':,}
            
            if token is not None, use it instead of password to authenticate
        
        """
        self.seaf = SeafileClient(hostdesc['url'])
        self.seaf.ping()
        if hostdesc.get('cookies', None):
            cookies_str = hostdesc['cookies']
            cookies_dict = {}
            for c in cookies_str.split(';'):
                c_name, c_value = c.split('=')
                cookies_dict[c_name.strip()] = c_value.strip()
            self.seaf.authenticate_cookies(cookies_dict, fetch_email=True)
        elif hostdesc.get('token', None):
            self.seaf.authenticate(hostdesc['email'], token=hostdesc['token'])
        else:
            self.seaf.authenticate(hostdesc['email'], password=hostdesc['password'])
        ctx.seaf.auth_ping()
        # clear current path and cache
        self.cwd = '/'
        self.repo_cache = []
        self.ls_cache = {}

    # directory operations

    def ls(self, path=None):
        """Return the list of nodes (repo, dir, or files) in `path`.
        
            If `path` is None, return the list of nodes in the cwd.
            
            nodes are {
                'name':,
                'id':,
                'type': repo|dir|file,
            }
            
            Once checked on seafile, each dir listing is cached.
        """
        if path is None:
            path = self.cwd
        parsed_path = self.parse(self.join(path))
        # root of all libraries
        if parsed_path['repo_id'] is None:
            if not self.repo_cache:
                self.repo_cache = self.seaf.list_repos()
            return [
                {
                    'name': repo['name'],
                    'id': repo['id'],
                    'type': repo['type'],
                } for repo in self.repo_cache
            ]
        else:
            
            repo_id, dirpath = parsed_path['repo_id'], parsed_path['path']
            try:
                ls = self.ls_cache[repo_id][dirpath]
            except KeyError:
                ls = self.seaf.list_dir(repo_id, dirpath)
            if not repo_id in self.ls_cache:
                self.ls_cache[repo_id] = {}
            if not dirpath in self.ls_cache[repo_id]:
                self.ls_cache[repo_id][dirpath] = ls
        # TODO: put the right .. status
        return [{'name': '..', 'id': None, 'type': 'dir'}] + [
            {
                'name': node['name'],
                'id': node['id'],
                'type' : node['type'],
            } for node in ls
        ]
    
    def cd(self, path='/'):
        """Change the cwd to the directory `path`
        
            .. is the parent dir
            / is the root of all libraries
        """
        # if relative path, join it with cwd
        if not path.startswith('/'):
            path = self.join(self.cwd, path)
        
        # to check the existence of the new cwd, we try to 'ls()' it
        # (also optimize further ls() by caching)
        self.ls(path)
        
        # if no exceptions raised, made it the new cwd
        self.cwd = path

    def mkdir(self, path):
        """Make the directory `path`"""
        # if relative path, join it with cwd
        if not path.startswith('/'):
            path = self.join(self.cwd, path)
        parsed_path = self.parse(path)
        repo_id, dirpath = parsed_path['repo_id'], parsed_path['path']
        print(self.seaf.make_dir(repo_id, dirpath))
        
        # invalidate the cache of `path`
        parent = '/'.join(dirpath.split('/')[:-1])
        try:
            del(self.ls_cache[repo_id][parent])
        except KeyError:
            pass

    # files operations
    def download(self, path, destination):
        """Download a file"""
        # if relative path, join it with cwd
        if not path.startswith('/'):
            path = self.join(self.cwd, path)
        parsed_path = self.parse(path)
        repo_id, filepath = parsed_path['repo_id'], parsed_path['path']
        filename = filepath.split('/')[-1]
        
        seaf_f = self.seaf.open_file(repo_id, filepath)
        with open(destination, 'wb') as out_f:
            shutil.copyfileobj(seaf_f, out_f)
        
        print('Downloaded ' + destination)
    
    def upload(self, localfile, path):
        """Upload the `localfile` to `path` in Seafile"""
        # if relative path, join it with cwd
        if not path.startswith('/'):
            path = self.join(self.cwd, path)
        
        parsed_path = self.parse(path)
        repo_id, parent = parsed_path['repo_id'], parsed_path['path']
        
        if repo_id is None:
            raise InvalidPath
        
        with open(localfile, 'rb') as fileo:
            fileid = self.seaf.upload_file(repo_id, parent, fileo)
        print('up, id =', fileid)
        # invalidate the cache of `path`
        try:
            del(self.ls_cache[repo_id][parent])
        except KeyError:
            pass
    
    def update(self, localfile, distfile):
        """Update the `localfile` to `distfile` in Seafile"""
        # if relative path, join it with cwd
        if not distfile.startswith('/'):
            distfile = self.join(self.cwd, distfile)
            
        parsed_path = self.parse(distfile)
        repo_id, filepath = parsed_path['repo_id'], parsed_path['path']
        
        if repo_id is None:
            raise InvalidPath
        
        with open(localfile, 'rb') as fileo:
            fileid = self.seaf.update_file(repo_id, filepath, fileo)
        print('updated, id =', fileid)
        
    def delete(self, path):
        """Delete a file or a directory"""
        # if relative path, join it with cwd
        if not path.startswith('/'):
            path = self.join(self.cwd, path)
        parsed_path = self.parse(path)
        repo_id, filepath = parsed_path['repo_id'], parsed_path['path']
        
        self.seaf.delete_file(repo_id, filepath)
        
        # invalidate the cache of `path/..`
        parent = self.join(path, '..')
        parent_path = self.parse(parent)['path']
        try:
            del(self.ls_cache[repo_id][parent_path])
        except KeyError:
            pass
    
    def stat(self, path):
        """Return data about a file or directory"""
        # if relative path, join it with cwd
        if not path.startswith('/'):
            path = self.join(self.cwd, path)
        parsed_path = self.parse(path)
        repo_id, filepath = parsed_path['repo_id'], parsed_path['path']
        
        return self.seaf.stat_file(repo_id, filepath)
        
    def share_link(self, path): 
        """Make a share link""" 
        # if relative path, join it with cwd 
        if not path.startswith('/'): 
            path = self.join(self.cwd, path) 
        parsed_path = self.parse(path) 
        repo_id, filepath = parsed_path['repo_id'], parsed_path['path'] 
        res = self.seaf.create_share_link(repo_id, filepath)
        
        return res['link']
    
    def edit_link(self, path): 
        """Make an edit link""" 
        # if relative path, join it with cwd 
        if not path.startswith('/'): 
            path = self.join(self.cwd, path) 
        parsed_path = self.parse(path) 
        repo_id, filepath = parsed_path['repo_id'], parsed_path['path'] 
        res = self.seaf.create_multi_share_link(repo_id, filepath, can_edit=True)
        return res['link']
    
    def get_share_links(self, path):
        """Return existing share links"""
        # if relative path, join it with cwd 
        if not path.startswith('/'): 
            path = self.join(self.cwd, path)  
        parsed_path = self.parse(path)  
        repo_id, filepath = parsed_path['repo_id'], parsed_path['path']
        res = self.seaf.share_links(repo_id, filepath)
        return res
    
    def get_or_make_share_link(self, path):
        """Return a share link, existing or new"""
        # if relative path, join it with cwd 
        if not path.startswith('/'): 
            path = self.join(self.cwd, path)   
        parsed_path = self.parse(path)   
        repo_id, filepath = parsed_path['repo_id'], parsed_path['path'] 
        res = self.seaf.get_or_create_share_link(repo_id, filepath) 
        return res

    def owa(self, path):
        """Return Office web app link for a file""" 
        # if relative path, join it with cwd 
        if not path.startswith('/'): 
            path = self.join(self.cwd, path) 
        parsed_path = self.parse(path) 
        repo_id, filepath = parsed_path['repo_id'], parsed_path['path'] 
         
        return self.seaf.office_web_app(repo_id, filepath, edit=True)

    # user operation
    
    def get_user_list(self):
        """Return user list"""
        return self.seaf.list_users(1, -1)
    
    def get_my_profile(self):
        """Return current user profile"""
        return self.seaf.user_my_profile()

    def init_user(self, email):
        """Initialize an ldap user"""
        return self.seaf.init_seafile_account(email)

    def add_user(self, email, password):
        """Add an user"""
        return self.seaf.add_user(email, password)
    
    def migrate_account(self, old_email, new_email):
        """Migrate an account"""
        self.seaf.migrate_account(old_email, new_email)

def seaferr_protected(f):
    """Decorator to catch Seafile errors"""
    @wraps(f)
    def wrapper(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except SeafileError as err:
            print(err)
    return wrapper

class SeafileShell(cmd.Cmd):
    intro = 'Welcome to the Seafile shell.   Type help or ? to list commands.\n'
    prompt = '(seafsh)$ '
    
    def __init__(self, browser):
        cmd.Cmd.__init__(self)
        self.ctx = browser
    
    def _update_prompt(self):
        """Update the prompt according to ctx"""
        if self.ctx.seaf is None:
            self.prompt = '(seafsh)$ '
        elif self.ctx.cwd is None:
            self.prompt = '(%s)$ ' % self.ctx.seaf.email
        else:
            self.prompt = '(%s)%s$ ' % (
                self.ctx.seaf.email,
                self.ctx.cwd
            )
    
    def do_exit(self, args=None):
        """Exit the shell"""
        return True
    
    def do_connect(self, hostname):
        """Connect and authenticate to `hostname`"""
        try:
            hostdesc = ctx.knownhosts[hostname]
        except KeyError as err:
            print('Unknown hostname: ' + str(err))
            return
        self.ctx.connect(hostdesc)
        print('auth, token=', ctx.seaf.token)
        self._update_prompt() 
    
    def complete_connect(self, text, line, begidx, endidx):
        """Completion for connect"""
        # return all hostnames starting with text
        return [
            hostname
            for hostname in self.ctx.knownhosts
            if hostname.startswith(text)
        ]
    
    def completedefault(self, text, line, begidx, endidx):
        """Completion for non-specific commands, provide file-system completion"""        
        # get the basepath for completion
        line = line[:begidx]
        if line.endswith('/'):
            # last part of line is part of basepath
            path = shlex.split(line)[-1]
            if path.startswith('/'):
                # absolute
                basepath = path
            else:
                # relative
                basepath = self.ctx.join(self.ctx.cwd, path)
        else:
            basepath = self.ctx.cwd
                
        return [
            (node['name'] + ('' if node['type'] == 'file' else '/'))
            for node in self.ctx.ls(basepath)
            if node['name'].startswith(text)
        ]
    
    @seaferr_protected
    def do_ls(self, *args):
        """List files or repo in the current place"""
        if not args[0]:
            path = self.ctx.cwd
        else:
            path = args[0]
        self.columnize([
            node['name'] for node in self.ctx.ls(path)
        ])
    
    @seaferr_protected
    def do_cd(self, path):
        """Go to path"""
        if path is None:
            path = '/'
        self.ctx.cd(path)
        self._update_prompt()

    @seaferr_protected
    def do_mkdir(self, path):
        """Make dir `path`"""
        if path is None:
            path = '/'
        self.ctx.mkdir(path)
        
    @seaferr_protected
    def do_download(self, path):
        """Download a file"""
        self.ctx.download(path, os.path.join(os.path.expanduser('~'), filename))
    
    @seaferr_protected
    def do_upload(self, local):
        """Upload a local file to the cwd"""
        self.ctx.upload(local, self.ctx.cwd)
    
    def complete_upload(self, text, line, begidx, endidx):
        """Completion using the local filesystem"""
        # get the basepath for completion
        line = line[:begidx]
        if line.endswith('/'):
            # last part of line is part of basepath
            path = shlex.split(line)[-1]
            if path.startswith('/'):
                # absolute
                basepath = path
            else:
                # relative
                basepath = os.path.join(os.curdir, path)
        else:
            basepath = os.curdir
        basepath = os.path.expanduser(basepath)
        return [ 
            fname for fname in os.listdir(basepath) if fname.startswith(text)
        ]
    
    @seaferr_protected
    def do_rm(self, path):
        """Delete a file or a directory"""
        self.ctx.delete(path)
    
    @seaferr_protected
    def do_rmdir(self, path):
        """Delete a file or a directory"""
        self.ctx.delete(path)
    
    @seaferr_protected
    def do_addline(self, path):
        """Add a line in an textfile"""
        # tmp filename
        tmp = os.path.join(os.path.expanduser('~'), 'seafsh.tmp')
        # download path
        self.ctx.download(path, tmp)
        # open and add line
        with open(tmp, 'a') as f:
            f.write('newline\n')
        # udate file
        self.ctx.update(tmp, path)
        # delete tmp
        os.remove(tmp)
    
    @seaferr_protected
    def do_stat(self, path):
        """Print details about path"""
        print(self.ctx.stat(path))

    @seaferr_protected
    def do_create_group(self, groupname):
        """Create a group"""
        print(self.ctx.seaf.create_group(groupname))
    
    @seaferr_protected
    def do_transfer_group(self, args):
        """Transfer a group group_id to new_owner"""
        try:
            group_id, new_owner = shlex.split(args)
        except IndexError:
            print("Usage: transfer_group group_id new_owner")
            return
        print(self.ctx.seaf.transfer_group(group_id, new_owner))
    
    @seaferr_protected
    def do_add_user_in_group(self, args): 
        """Add an user in the group group_id""" 
        try: 
            group_id, new_user = shlex.split(args) 
        except IndexError: 
            print("Usage: add_user_in_group group_id new_user") 
            return 
        print(self.ctx.seaf.add_user_in_group(group_id, new_user)) 
    
    @seaferr_protected
    def do_set_group_admin_status(self, args):
        """Set admin status of group_id"""
        try:
            group_id, user, admin_status = shlex.split(args)
        except IndexError:
            print("Usage: set_group_admin_status group_id user [true|false]")
            return
        admin_status = (admin_status == 'true')
        print(self.ctx.seaf.set_group_admin_status(group_id, user, admin_status))
    
    @seaferr_protected
    def do_share_link(self, path):
        """Create a share link for path"""
        print(self.ctx.share_link(path))

    @seaferr_protected                                                           
    def do_edit_link(self, path):                                               
        """Create an edit link for path"""                                       
        print(self.ctx.edit_link(path))

    @seaferr_protected
    def do_get_share_links(self, path):
        """Print existing share links for path"""
        print(self.ctx.get_share_links(path))
    
    @seaferr_protected
    def do_get_or_make_share_link(self, path):
        """Get a existing share link or make a new one"""
        print(self.ctx.get_or_make_share_link(path))

    @seaferr_protected
    def do_get_user_list(self, path):
        """Return user list"""
        print(pprint(self.ctx.get_user_list()))

    @seaferr_protected
    def do_get_my_profile(self, path):
        """Return my profile"""
        print(pprint(self.ctx.get_my_profile()))
    
    @seaferr_protected
    def do_add_user(self, args):
        """Create an user"""
        try:                                                                     
            email, password = shlex.split(args)                              
        except IndexError:                                                       
            print("Usage: email password")                    
            return
        print(pprint(self.ctx.add_user(email, password)))

    @seaferr_protected
    def do_init_user(self, username):
        """Initialize an account (only LDAP account Seafile v<11)"""
        print(self.ctx.init_user(username))

    @seaferr_protected
    def do_owa(self, args):
        """Get OWA data for a file"""
        print(pprint(self.ctx.owa(args)))
    
    @seaferr_protected
    def do_migrate_account(self, args):
        """Migrate an account"""
        try:
            old_email, new_email = shlex.split(args)
        except IndexError:
            print("Usage: old_email new_email")
            return
        self.ctx.migrate_account(old_email, new_email)

if __name__ == '__main__':
    
    conf_name = os.path.join(
        os.path.expanduser('~'),
        '.config/seafsh-knownhosts.json'
    )
    
    try:
        with open(conf_name, 'r') as conf:
            knownhosts = json.load(conf)
    except FileNotFoundError:
        print('You must create the file %s with known host configuration.' % 
                conf_name)
        print('Sample:')
        print('''
{
    "host1": {
        "url": "https://example.com/",
        "email": "user@example.com",
        "password": "1234"
    },
    "host2": {
        "url": "https://example.org/seafile/",
        "email": "user@example.org",
        "token": "38516201a16a48ab9585b082e8cb49a6b3ec1234",
    }
}
''')
        print('After the first password authentication, you can remove your')
        print('password from this file and put the token instead.')
    else:
        ctx = SeafBrowser(knownhosts)    
        SeafileShell(ctx).cmdloop()
    

