# -*- coding: utf-8 -*-
###############################################################################
#       seafile_webapi.py
#       
#       Copyright © 2015-2025, Florence Birée <florence@biree.name>
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU Affero General Public License as 
#       published by the Free Software Foundation, either version 3 of the 
#       License, or (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU Affero General Public License for more details.
#       
#       You should have received a copy of the GNU Affero General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Seafile web API wrapper"""

__author__ = "Florence Birée"
__version__ = "0.12"
__license__ = "AGPLv3"
__copyright__ = "Copyright © 2015-2025, Florence Birée <florence@biree.name>"

import os
from functools import wraps
from urllib.parse import urljoin, urlparse, quote
import requests
from requests.exceptions import HTTPError
import json
import certifi
import urllib3

# HTTP verbs
GET = requests.get
POST = requests.post
PUT = requests.put
DELETE = requests.delete

# Seafile exceptions
class SeafileError(Exception):
    """Base class for all Seafile-related exceptions"""

class NotAuthenticated(SeafileError):
    """Raised when trying an operation that need authentication"""
    pass

class AuthError(SeafileError):
    """Authentification error"""
    pass

class APIError(SeafileError):
    """API error"""
    msg = "Seafile API error"
    def __init__(self, curl_cmd=None, msg=None, status_code=None):
        """APIError(curl_cmd, *args)"""
        self.curl_cmd = curl_cmd
        if msg:
            self.msg = msg
        if status_code:
            self.status_code = status_code
    
    def __add__curl(self, msg, code):
        if self.curl_cmd:
            return "{}: {}".format(msg, code) + '\n$ ' + self.curl_cmd
        else:
            return "{}: {}".format(msg, code)
    
    def __str__(self):
        return self.__add__curl(self.msg, self.status_code)

class BadPath(APIError):
    code = 400
    msg = "400 Path is missing/bad."

class InvalidToken(APIError):
    code = 401
    msg = "401 Invalid Token"

class Forbidden(APIError):
    code = 403
    msg = "403 Forbidden"

class NotFound(APIError):
    code = 404
    msg = "404 The path does not exists."

class InvalidPath(APIError):
    code = 440
    msg = "440 Invalid path or filname, or encrypted repo."

class FileExists(APIError):
    code = 441
    msg = "441 File already exists."

class InternalServerError(APIError):
    code = 500
    msg = "500 Internal server error (may be out of quota)."

class OperationFailed(APIError):
    code = 520
    msg = "520 Operation failed."

EXCEPT_CODE = {
    400: BadPath,
    401: InvalidToken,
    403: Forbidden,
    404: NotFound,
    440: InvalidPath,
    441: FileExists,
    500: InternalServerError,
    520: OperationFailed,
}

def curlify(request):
    """Return a curl command line corresponding to the `request`"""
    return (
        'curl -v ' +
        ('-X PUT ' if request.method == 'PUT' else '') +
        (('-d "%s" ' % request.body) if request.body else '') +
        ' '.join("-H '%s: %s'" % (k, v) for (k, v) in request.headers.items()) +
        ' ' +
        request.url
    )

class SeafileClient:
    """Seafile connector"""
    
    base_api = 'api2/'
    base_api21 = 'api/v2.1/'
    
    # internals
    
    def __init__(self, url, verifycerts=True, v12auth=False):
        """New seafile connector to `url` instance
        
            set verifycerts to False to disable TLS certificate verification
            
            if your serveur use Seafile v12+, set v12auth=True
        """
        self.url = url
        self._api_url = urljoin(self.url, self.base_api)
        self._api_url21 = urljoin(self.url, self.base_api21)
        self.token = None
        self.email = None
        self.verify = verifycerts
        self.v12auth = v12auth
    
    def _need_auth(func):
        """Decorator to ensure the connector is authentified before 
        executing `func`"""
        @wraps(func)
        def wrapper(self, *args, **kwargs):
            if self.token is None:
                raise NotAuthenticated
            return func(self, *args, **kwargs)
        return wrapper
    
    def __repr__(self):
        """String representation of the object"""
        hostname = urlparse(self.url).hostname
        if self.token is None:
            return "<Seafile({hostname})>".format({'hostname': hostname})
        else:
            return "<Seafile({email}@{hostname})>".format({
                'email': self.email,
                'hostname': hostname
            })
    
    def _api(self, verb, cmd, params=None, data=None, headers=None, files=None,
                   token=True, raw_url=False, api21=False, debug=False, 
                   cookies=None, json_data=False):
        """Execute the API command VERB `cmd`
        
            VERB is one of GET, POST, PUT, DELETE
        
            `params` : optionals ?= params
            `headers` : optionals headers
            `data` : optionals POST or PUT data
            `files` : POST data content using the Content-Type                  
                multipart/form-data (RFC 2388). If a value in the data dict is   
                an opened file object, it will be sent as a file.
            
            if `token`, add the Authorization header
            if `raw_url`, use `cmd` as full url instead of concatenating `cmd`
                to the API url.
            
            if `api21` is not None, use API2.1 instead of API2.
            
            if `debug` is True, return the curl command line of the request
                instead of normal behaviour
            
            if `cookies` (dict or RequestsCookieJar), pass cookies to the
                request
            
            if `json_data`, use requests POST(json=) argument for data instead
                of data
            
            Return json-loaded data
        """
        final_headers = {'Accept': 'application/json; indent=4; charset=utf-8'}
        if headers:
            final_headers.update(headers)
        if token:
            final_headers['Authorization'] = (
                ('Bearer ' if self.v12auth else 'Token ') + self.token
            )
        
        if raw_url:
            url = cmd
        elif api21:
            url = urljoin(self._api_url21, cmd)
        else:
            url = urljoin(self._api_url, cmd)
            
        if json_data:
            r = verb(
                url,
                params=params,
                headers=final_headers,
                json=data,
                files=files,
                verify=self.verify,
                cookies=cookies,
            )
        else:
            r = verb(
                url,
                params=params,
                headers=final_headers,
                data=data,
                files=files,
                verify=self.verify,
                cookies=cookies,
            )
        if debug:
            return curlify(r.request)
        try:
            r.raise_for_status()
        except HTTPError:
            apierror = EXCEPT_CODE.get(r.status_code, APIError)
            raise apierror(
                curlify(r.request),
                r.text,
                r.status_code,
            )
        else:
            return r.json()
    
    def _multipart_filname_patching(self, prepped, filename):
        """Since Seafile doesn't handle well RFC2231
            wich specify to send filename*= field for utf-8 characters
            (which is what Requests does), we patch the request body
            to put just a raw utf-8 filename (like curl does)
        
            this is a dirty hack
            
            `prepped` is a prepared request
            `filename` is the str filename to encode in raw utf-8
            
            return the patched prepped
        """
        starbytes = b'filename*='
        # find the start of starbytes
        start = prepped.body.find(starbytes)
        if start == -1:
            # not here, return prepped unchanged
            return prepped
        # find the first \r\n sequence after starbytes (end of filename)
        end = prepped.body.find(b'\r\n', start)
        
        # patch the body
        prepped.body = (
            prepped.body[:start] +
            b'filename="' + filename.encode('utf8') + b'"' +
            prepped.body[end:]
        )
        # recompute the Content-Length header
        prepped.headers['Content-Length'] = str(len(prepped.body))
        return prepped

    # auth methods
    
    def authenticate(self, email, password=None, token=None, validate=True):
        """Authenticate against the Seafile server, with `email` and either:
            - `password` : to authenticate with a password
            - `token` : to reuse a token from a previous authentication
            
            !! `email` can also be the `login_id` or the `contact_email` of the user
            
            if success, the token is available at self.token
            else raise AuthError
            
            if not validate, the Seafile connector will not check the validy of
                the token (if token authentication).
                No seafile request will be done. If you are sure about your
                token validy, this will save time.
        """
        if password is None and token is None:
            raise ValueError
        elif token is not None: # token auth
            if validate:
                # try to validate the token
                try:
                    self._auth_ping(token)
                except SeafileError:
                    raise AuthError
                else:
                    self.token = token
                    self.email = email
            else:
                self.token = token
                self.email = email
        else: # password auth
            try:
                resp = self._api(POST, 'auth-token/', data={
                    'username': email,
                    'password': password
                }, token=False)
            except:
                raise AuthError
            else:
                if resp['token']:
                    self.token = resp['token']
                    self.email = email
    
    def authenticate_cookies(self, cookies, fetch_email=False):
        """Authenticate using cookies retrived by a real Seafile authenication
        
        Using the cookiejar `cookies`, get the web API token for this user,
        which will be used for furthur connexion. If no web API token, create 
        one.
        
        this method handle regular seafile authentication as well as
        social (oauth) authentication.
        
        This allow to authenticate without knowing the email ID of the user.
        if `fetch_email` is False, the self.email field will be None
        if `fetch_email` is True, a second request to the API will be used
            to populate the self.email field
        """

        # try to get the existing web API token
        try:
            resp = self._api(GET, 'auth-token-by-session/',                     
                headers={
                    'X-CSRFToken': cookies['sfcsrftoken'],
                    'Referer': self.url,
                },
                cookies=cookies,                                                 
                token=False,                                                     
                api21=True,                                                      
            )
        except:
            raise AuthError
        
        if resp['token'] == '':
            # no existing web API token, create one
            try:
                resp = self._api(POST, 'auth-token-by-session/',
                    headers={
                        'X-CSRFToken': cookies['sfcsrftoken'],
                        'Referer': self.url,
                    },
                    cookies=cookies,
                    token=False,
                    api21=True,
                )
            except:
                raise AuthError
            
        if resp['token']: 
            self.token = resp['token']
            if fetch_email:
                self.email = self.user_my_profile()['email'] 
    
    # test methods
    
    def ping(self):
        """Ping the Seafile server
        
            raise SeafileError if not working
        """
        if not self._api(GET, 'ping/', token=False) == "pong":
            raise SeafileError
        
    def _auth_ping(self, token):
        """Ping the Seafile server with the token `token`
        
            raise SeafileError if not working
        """
        # here the token may not be validated, so we do not use _api(token=True)
        headers = {'Authorization': 'Token ' + token}
        resp = self._api(GET, 'auth/ping/', headers=headers, token=False)
        if resp != "pong":
            raise SeafileError
        
    @_need_auth
    def auth_ping(self):
        """Ping the Seafile server with the token `token`
        
            raise SeafileError if not working
        """
        self._auth_ping(self.token)
    
    # library methods
    
    @_need_auth
    def list_repos(self):
        """List repos/librarys
            
            return a list of {
                "permission": "rw",
                "encrypted": false,
                "mtime": 1400054900,
                "owner": "user@mail.com",
                "id": "f158d1dd-cc19-412c-b143-2ac83f352290",
                "size": 0,
                "name": "foo",
                "type": "repo",
                "virtual": false,
                "desc": "new library",
                "root": "0000000000000000000000000000000000000000"
            }
        """
        return self._api(GET, 'repos/')

    @_need_auth
    def repo_info(self, repo_id):
        """Return info about `repo_id` :
            {
                "permission": "rw",
                "encrypted": false,
                "mtime": 1572659621,
                "owner": "self",
                "modifier_contact_email": "foo@foo.com",   
                "id": "11894b06-7b48-4ec5-ac11-a0da45d7ee70",   
                "modifier_name": "foo", 
                "size": 127696753,
                "modifier_email": "foo@foo.com",
                "name": "lib of foo",
                "root": "ebc2aad269e11a2123d3bb17aca6096651dee18c",
                "file_count": 5,
                "type": "repo"
            }
        """
        return self._api(GET, 'repos/{repo_id}/'.format(repo_id=repo_id))

    # directory methods

    @_need_auth 
    def list_dir(self, repo_id, path="/"): 
        """list the content of a directory from library `repo_id`/`path`
        
            return a list of {
                "id": "e4fe14c8cda2206bb9606907cf4fca6b30221cf9",
                "type": "file|dir",
                "name": "test",
                "size": 0, # only for files
            }
            
            Errors:
                404 NotFound
                440 InvalidPath (encrypted repo)
                520 OperationFailed
        """
        return self._api(
            GET,
            'repos/{repo_id}/dir/'.format(repo_id=repo_id),
            {'p': path}
        )
    
    @_need_auth
    def make_dir(self, repo_id, path="/"):
        """make the directory `repo_id`/`path`
        
            return the location URL of the directory
            
            Errors:
                400 BadPath 
                520 OperationFailed
        """
        return self._api(
            POST,
            'repos/{repo_id}/dir/'.format(repo_id=repo_id),
            {'p': path},
            {'operation': 'mkdir'},
        )
    
    # files methods
    
    @_need_auth
    def open_file(self, repo_id, path):
        """get the file `repo_id`/`path`.
        
            Return an opened requests.Response.raw file-like object
            
            Errors:
                400 BadPath
                404 NotFound 
                520 OperationFailed
        """
        # get the file link
        flink = self._api(
            GET,
            'repos/{repo_id}/file/'.format(repo_id=repo_id),
            {'p': path}
        )
        resp = requests.get(flink, stream=True)
        resp.raw.decode_content = True
        return resp.raw
    
    @_need_auth
    def lock_file(self, repo_id, path):
        """lock the file `repo_id`/`path`.
        
            Not implemented server side.
        """
        raise NotImplementedError
        #return (self._api_cmd(
        #    'repos/{repo_id}/file/'.format(repo_id=repo_id),
        #    {'operation': 'lock', 'p': path},
        #    put=True
        #) == "success")
    
    @_need_auth
    def unlock_file(self, repo_id, path):
        """unlock the file `repo_id`/`path`.
        
            Not implemented server side.
        """
        raise NotImplementedError
        #return (self._api_cmd(
        #    'repos/{repo_id}/file/'.format(repo_id=repo_id),
        #    {'operation': 'unlock', 'p': path},
        #    put=True
        #) == "success")
    
    @_need_auth
    def upload_file(self, repo_id, parent, fileo):
        """upload the file object `fileo` to `repo_id`/`parent`
        
            Return the file id
            
            Errors:
                400 BadPath
                440 InvalidPath
                441 FileExists
                500 InternalServerError (out of quota)
        """
        # get upload link
        up_link = self._api(GET,
            'repos/{repo_id}/upload-link/'.format(repo_id=repo_id),
            {'p': parent}
        )
        
        filename = os.path.split(fileo.name)[1]
        # upload file
        s = requests.Session()
        req = requests.Request('POST',
            up_link,
            files={
                'filename': (None, filename),
                'parent_dir': (None, parent),
                'file': (filename, fileo, 'application/octet-stream'),
            },
            #verify=self.verify
        )
        req.verify=self.verify
        prepped = self._multipart_filname_patching(req.prepare(), filename)
        
        r = s.send(prepped, stream=False)
        try:
            r.raise_for_status()
        except HTTPError:
            apierror = EXCEPT_CODE.get(r.status_code, APIError)
            raise apierror(
                curlify(r.request),
                r.text
            )
        else:
            return r.text
    
    @_need_auth
    def update_file(self, repo_id, filepath, fileo):
        """update the file `repo_id`/`filepathe` with the file object `fileo`
        
            Return the file id
            
            Errors:
                400 BadPath
                440 InvalidPath
                500 InternalServerError (out of quota)
        """
        # get update link
        up_link = self._api(GET,
            'repos/{repo_id}/update-link/'.format(repo_id=repo_id)
        )
        
        try:
            filename = os.path.split(fileo.name)[1]
        except AttributeError:
            filename = os.path.split(filepath)[1]
        
        # upload file
        s = requests.Session()
        req = requests.Request('POST',
            up_link,
            files={
                #'filename': (None, filename),
                'target_file': (None, filepath),
                'file': (filename, fileo, 'application/octet-stream'),
            }
        )
        prepped = self._multipart_filname_patching(req.prepare(), filename)
        
        r = s.send(prepped, stream=False, verify=self.verify)
        try:
            r.raise_for_status()
        except HTTPError:
            apierror = EXCEPT_CODE.get(r.status_code, APIError)
            raise apierror(
                curlify(r.request),
                r.text
            )
        else:
            return r.text
    
    
    @_need_auth
    def delete_file(self, repo_id, path):
        """delete the file or directory `repo_id`/`path`
        
            Errors:
                400 BadPath
                520 OperationFailed
        """
        return (self._api(DELETE,
            'repos/{repo_id}/file/'.format(repo_id=repo_id),
            params={'p': path}
        ) == "success")

    @_need_auth
    def stat_file(self, repo_id, path):
        """get data about the file `repo_id`/`path`
        
            Return {'id', 'mtime', 'type':'file', 'name', 'size'}
            
            Errors:
                400 BadPath
                520 OperationFailed
        """
        return self._api(GET,
            'repos/{repo_id}/file/detail/'.format(repo_id=repo_id),
            params={'p': path}
        )

    @_need_auth
    def office_web_app(self, repo_id, path, edit=False):
        """Return data to view/edit a file through an office web app (wopi
            protocole. (Pro edition only)
            
            return {'access-token', 'action_url', 'access_token_ttl'}
            see https://download.seafile.com/published/web-api/v2.1/office-web-app-integration.md
        """
        return self._api(GET,
            'repos/{repo_id}/owa-file/'.format(repo_id=repo_id),
            params={'path': path, 'action': 'edit' if edit else 'view'},
        )

    # share link methods
    
    @_need_auth
    def share_links(self, repo_id=None, path=None):
        """Return the list of existing share links
        
            if repo_id is specified, return only the list of this repo
            if path is specified, return only the list of this folder
            
            Return {"username", "repo_id", "ctime", "expire_date", "token",
                    "view_cnt", "link", "obj_name", "path", "is_dir", 
                    "is_expired", "repo_name"}
            Errors:
                403 Forbidden
                500 InternalServerError
        """
        params = {}
        if repo_id is not None:
            params['repo_id'] = repo_id
            if path is not None:
                params['path'] = path
        return self._api(GET,
            'share-links/',
            params=params,
            api21=True,
        )
    
    @_need_auth
    def create_share_link(self, repo_id, path, cloud_edit=False):
        """Return a share link for a file or a folder
        
        set cloud_edit=True to generate a cloud edit link
        
        Return {"username", "repo_id", "ctime", "expire_date", "token",
                "view_cnt", "link", "obj_name", "path", "is_dir", "permissions",
                "is_expired", "repo_name"}
        
        Errors:
            400 path/repo_id invalid
            403 Permission denied.
            404 file/folder/library not found.
            500 Internal Server Error
        """
        if cloud_edit:
            return self._api(                                                        
                POST,                                                                 
                'share-links/', 
                {}, 
                { 
                    'repo_id': repo_id, 
                    'path': path,
                    'permissions': {
                        'can_download': True,
                        'can_edit': True,
                        'can_upload': False,
                    },
                },                                                          
                api21=True,
                json_data=True,
            )
        else:
            return self._api(                                                        
                POST,                                                                
                'share-links/',
                {},
                {
                    'repo_id': repo_id,
                    'path': path,
                },                                                         
                api21=True,
            )
    
    @_need_auth
    def create_multi_share_link(self, repo_id, path, can_download=True,
            can_edit=False, can_upload=False):
        """Return a share link for a file or a folder
        
        Return {"username", "repo_id", "ctime", "expire_date", "token",
                "view_cnt", "link", "obj_name", "path", "is_dir", "permissions",
                "is_expired", "repo_name"}
        
        """
        return self._api(                                                        
            POST,                                                                 
            'multi-share-links/', 
            {}, 
            { 
                'repo_id': repo_id, 
                'path': path,
                'permissions': {
                    'can_download': can_download,
                    'can_edit': can_edit,
                    'can_upload': can_upload,
                },
                'user_scope': 'all_users'
            },                                                          
            api21=True,
            json_data=True,
        )
        
    @_need_auth
    def get_or_create_share_link(self, repo_id, path):
        """Return an existing share_link or create one"""
        share_link_infos = self.share_links(repo_id, path)
        if share_link_infos and 'link' in share_link_infos[0]:
            return share_link_infos[0]['link']
        share_link_infos = self.create_share_link(repo_id, path)
        return share_link_infos['link']
    
    def internal_file_link(self, repo_id, path):
        """Return an internal link for a file"""
        url = '{url}/lib/{repo_id}/file{path}'.format(
            url=self.url,
            repo_id=repo_id,
            path=path
        )
        url = quote(url, safe="/#")
        return url.replace('//', '/').replace('https%3A/', 'https://')
    
    def internal_dir_link(self, repo_id, path):
        """Return an internal link for a dir"""
        url = '{url}/#my-libs/lib/{repo_id}{path}'.format(
            url=self.url,
            repo_id=repo_id,
            path=path
        )
        url = quote(url, safe="/#")
        return url.replace('//', '/').replace('https%3A/', 'https://')
    
    # users and groups methods
    def _post_csrf(self, url, *args, **kwargs):
        """Send a POST request wit a CSRF cookie"""
        session = requests.Session()
        r = session.get(urljoin(self.url, url)) #, verify=False)
        csrf = r.cookies.get("csrftoken", r.cookies["sfcsrftoken"])
        if "headers" not in kwargs.keys():
            kwargs["headers"] = dict()
        kwargs["headers"]['Referer'] = "{}{}".format(self.url, url)
        if "data" not in kwargs.keys():
            kwargs["data"] = dict()
        kwargs["data"]['csrfmiddlewaretoken'] = csrf
        return session.post(urljoin(self.url, url), *args, **kwargs) #, verify=False, **kwargs)

    def init_seafile_account(self, username):
        """Make a fake login to `username`, to create the user entry in the
            Seafile database (LDAP).
            
            Do not works with SSO users, see add_user() to do the same in
            this case.
            
            This method works only for Seafile v<11.
            See add_user() for Seafile 11+.
        """
        try:
            return self._post_csrf("accounts/login/", data={
                "login": username,
                "password": "NONE",
                "next": "/seafile/"
            }).status_code == requests.codes.ok
        except KeyError:
            # cookie sfcsrftoken not found (???)
            raise OperationFailed(msg="Cookie sfcsrftoken not found")

    @_need_auth
    def create_group(self, group_name):
        """Create a new group `group_name`
        get data about the file `repo_id`/`path`
        
            Return {'name', 'owner', 'created_at', 'admins', 'id'}
            
            Error:
                400 BadPath (already a group with that name)
            
        """
        return self._api(POST, 'groups/', data={
            'name': group_name,
        }, api21=True)
        
    
    @_need_auth
    def transfer_group(self, group_id, new_owner, admin=False):
        """Transfer the property of the group `group_id` to `new_owner`
        
            If `admin` is True, perform this operation as admin
        
            Return updated group informations
        """
        if admin:
            return self._api(PUT, 
                'admin/groups/{}/'.format(group_id),
                data={
                    'new_owner': new_owner,
                },
                api21=True
            )
        else:
            return self._api(PUT, 'groups/{}/'.format(group_id), data={
                'owner': new_owner,
            }, api21=True)

    @_need_auth
    def add_user_in_group(self, group_id, user, admin=False):
        """Add an user in the group `group_id`.
        
        You can also specify a comma-delimited user list to make a bulk add
        If admin, perform this operation as seafile admin
        
            Return group data
        """
        if admin:
            for u in user.split(','):
                res = self._api(POST, 
                    'admin/groups/{}/members/'.format(group_id),
                    data={'email': u},
                    api21=True
                )
            return res
        
        # not admin
        if ',' in user:
            # bulk add
            return self._api(POST, 'groups/{}/members/bulk/'.format(group_id),
                data={
                    'emails': user,
                }, api21=True)
        else:
            return self._api(POST, 'groups/{}/members/'.format(group_id), data={
                'email': user,
            }, api21=True)
        

    @_need_auth
    def set_group_admin_status(self, group_id, user, admin_status=True, admin=False):
        """Set the group admin status of user
        
        If admin, perform as Seafile administrator
        """
        is_admin = ('true' if admin_status else 'false')
        if admin:
            return self._api(PUT,
                'admin/groups/{}/members/{}/'.format(group_id, user),
                data={
                    'is_admin': is_admin,
                }, api21=True
            )
        else:
            return self._api(PUT, 
                'groups/{}/members/{}/'.format(group_id, user),
                data = {'is_admin': is_admin},
                api21=True
            )

    @_need_auth
    def search_group(self, group_name, admin=False):
        """Search for a group `group_name`
        
        If admin, perform as Seafile administrator
        
        Return:
            [
                {
                    "id": 1,
                    "name": "group of foo",
                    "owner": "foo@foo.com",
                    "owner_name": "name-of-foo",
                    "created_at": "2019-11-15T17:21:18+08:00",
                    "quota": -1,
                    "parent_group_id": 0
                },
                ...
            ]
        """
        result = self._api(GET,
            'admin/search-group/' if admin else 'search-group/',
            params={'query': group_name, 'q':group_name},
            api21=True
        )
        if isinstance(result, dict):
            return result['group_list']
        else:
            return result

    @_need_auth
    def user_my_profile(self):
        """Return data about the current logged user profile:
            
            {
                "login_id": "006", # can be used instead of email to authenticate 
                "name": "Jon Snow",
                "telephone": "110",
                "list_in_address_book": false,
                "contact_email": "othermailofjon@gmail.com", # to communicate
                "email": "jonsnow@gmail.com"    # internal seafile ID
            }
        """
        return self._api(GET, 'user/', api21=True)

    @_need_auth
    def add_user(self,
        email,
        password,
        is_staff=False,
        is_active=True,
        role="",
        name="",
        login_id="",
        contact_email="",
        reference_id="",
        departement="",
        quota_total="-2",
    ):
        """Add an user. email is the internal Seafile ID, can be a fake e-mail
        
            Need to be admin.
            
            Seafile v<11:
            To pre-populate oauth account, it's possible to create a new account,
            with the `email` in the form oauth will build, and a random strong
            password. The account info will be updated at the first oauth
            login.
            
            Seafile 11+:
            To pre-populate an account (any SSO), you must first create a local
            account using this method, and next use the companion module
            seafile_db to switch the authentification method for this account
            to the SSO you want, with the correct SSO uid.
            You also need to create the account with a random strong password,
            the password will be deleted during the switch to the SSO system.
        """
        return self._api(POST, 
            'admin/users/',
            data={
                'email': email,
                'password': password,
                'is_staff': is_staff,
                'is_active': is_active,
                'role': role,
                'login_id': login_id,
                'contact_email': contact_email,
                'reference_id': reference_id,
                'departement': departement,
                #'quota_total': quota_total
            },
            api21=True
        )
        
    ### Administration operation
    
    @_need_auth
    def list_users(self, page, per_page):
        """Get the list of users:
            
            `per_page` is the max number of users to return per page (-1 for all)
            `page` is the page you want to fetch
        """
        return self._api(GET, 'admin/users/',
            params={
                'page': page,
                'per_page': per_page,
            },
            api21=True
        )
    
    @_need_auth
    def migrate_account(self, old_email, new_email):
        """Migrate an account ID from `old_email` to `new_email`.
        
        The `new_email` account must exists.
        
        Need admin privileges
        """
        self._api(POST, 'accounts/{}/'.format(old_email),
            data={
                'op': 'migrate',
                'to_user': new_email,
            }
        )
    
    @_need_auth
    def delete_user(self, email):
        """Delete the user `email`"""
        self._api(DELETE, 'admin/users/{}/'.format(email), api21=True)

